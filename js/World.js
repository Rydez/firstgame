


var ShapeShifter;
(function(ShapeShifter) {

    var World = function () {
    	this.background;

    	// Platform factors
        this.platformHeightFrac = 0.04;
    	this.sidePlatformWidthFrac = 0.3;
    	this.groundWidthFrac = 0.6;

        this.ySidePlatformsFrac = 0.5;
    	this.yGroundFrac = 0.75;

    	// Platforms
    	this.groundPlatform;
    	this.leftPlatform;
    	this.rightPlatform;

    	this.platformGroup;
    };

    World.prototype = function () {

    	var preload = function (game) {
			game.load.image('background', 'assets/background.png');

			game.load.image('ground', 'assets/ground.png');
    	};

        var create = function (game) {

        	// Background
		    this.background = game.add.image(0, 0, 'background');
		    this.background.width = game.width;
		    this.background.height = game.height;

		    // Ground
		    this.groundPlatform = game.add.sprite(game.width/2,
                    this.yGroundFrac*game.height, 'ground');
		    this.groundPlatform.width = this.groundWidthFrac*game.width;
		    this.groundPlatform.height = this.platformHeightFrac*game.height;
            this.groundPlatform.anchor.setTo(0.5, 0.5)

		    // Left platform
		    this.leftPlatform = game.add.sprite(game.width/4,
                    this.ySidePlatformsFrac*game.height, 'ground');
		    this.leftPlatform.width = this.sidePlatformWidthFrac*game.width;
		    this.leftPlatform.height = this.platformHeightFrac*game.height;
		    this.leftPlatform.anchor.setTo(0.5, 0.5);

		    // Right platform
		    this.rightPlatform = game.add.sprite(3*game.width/4,
                    this.ySidePlatformsFrac*game.height, 'ground');
		    this.rightPlatform.width = this.sidePlatformWidthFrac*game.width;
		    this.rightPlatform.height = this.platformHeightFrac*game.height;
		    this.rightPlatform.anchor.setTo(0.5, 0.5);

		    game.physics.arcade.enable([
		    	this.groundPlatform,
		    	this.rightPlatform,
		    	this.leftPlatform
		    ]);

		    this.groundPlatform.body.immovable = true;
		    this.rightPlatform.body.immovable = true;
		    this.leftPlatform.body.immovable = true;

		    this.groundPlatform.body.moves = false;
		    this.rightPlatform.body.moves = false;
		    this.leftPlatform.body.moves = false;

		    this.platformGroup = game.add.group();
		    this.platformGroup.add(this.groundPlatform);
		    this.platformGroup.add(this.rightPlatform);
		    this.platformGroup.add(this.leftPlatform);
        };

        return {
            preload: preload,
            create: create
        };
    }();

    ShapeShifter.World = new World();

})(ShapeShifter || (ShapeShifter = {}));
