




var ShapeShifter;
(function(ShapeShifter) {

    var EnemyManager = function () {
        //
    };

    EnemyManager.prototype = function () {

    	var preload = function (game) {
    		ShapeShifter.GroundEnemy.preload(game);
    	};

        var create = function (game) {
    		ShapeShifter.GroundEnemy.create(game);

            this.allEnemieSingletons = [];
            this.allEnemieSingletons.push(ShapeShifter.GroundEnemy);

            this.allEnemieGroups = [];
            this.allEnemieGroups.push(ShapeShifter.GroundEnemy.groundEnemyGroup);
        };

        var update = function (game, player, world, waveManager) {
    		ShapeShifter.GroundEnemy.update(game, player, world, waveManager);

            // Return if an enemy is still alive
            if (waveManager.waveStarted) {

                for (var i = 0; i < this.allEnemieGroups.length; i++) {
                    var numberSpawned = this.allEnemieSingletons[i].numberSpawned;
                    // var numberToSpawn = this.allEnemieSingletons[i].numberOfEnemies;
                    var groupEnemies = this.allEnemieGroups[i].children;
                    for (var j = 0; j < groupEnemies.length; j++) {
                        if (waveManager.timeLeftOnWave >= 0) {
                            return;
                        }
                        if (groupEnemies[j].alive) {
                            return;
                        }
                    }
                }
                waveManager.endWave(game, this);
            }
        };

        var addEnemies = function (game, wave) {
            ShapeShifter.GroundEnemy.addEnemies(game, wave);
        };

        // var removeEnemies = function () {
        //     ShapeShifter.GroundEnemy.removeEnemies();
        // };

        return {
            preload: preload,
            create: create,
            update: update,
            addEnemies: addEnemies,
            // removeEnemies: removeEnemies
        };
    }();

    ShapeShifter.EnemyManager = new EnemyManager();

})(ShapeShifter || (ShapeShifter = {}));
