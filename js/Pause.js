

var ShapeShifter;
(function(ShapeShifter) {

    var Pause = function () {

    };

    Pause.prototype = function () {

        var create = function (game) {

        	var pauseButton = game.input.keyboard.addKey(Phaser.Keyboard.P)
    		pauseButton.onDown.add(function () {
			    if (game.paused) {
			    	removePauseMenu();
			        game.paused = false;
			    } 
			}, this);

            game.onPause.add(function () {
            	game.paused = true;
            	createPauseMenu(game);
            });
        };

        var resize = function (game) {
            game.paused = true;
            removePauseMenu();
            createPauseMenu(game);
        };

        // Private
    	var overlay;
    	var text;

    	var fontSizeFactor = 0.06;
    	var style = {
    		font: "Arial", 
    		fill: "#000000", 
    		align: "center" 
    	};
    	var message = "Paused. \n(Click P to resume!)";

        var createPauseMenu = function (game) {
        	overlay = game.add.graphics(0, 0);
        	overlay.lineStyle(0);
        	overlay.beginFill(0x000000, 0.3)
        	overlay.drawRect(0, 0, game.width, game.height);
        	overlay.endFill();

    		text = game.add.text(0.5*game.width, 0.25*game.height, 
    				message, style);

    		text.anchor.set(0.5);

    		if (game.width < game.height) {
    			text.fontSize = fontSizeFactor*game.width;
    		}
    		else {
    			text.fontSize = fontSizeFactor*game.height;
    		}
        };

        var removePauseMenu = function () {
        	overlay.destroy();
        	text.destroy();
        };

        return {
            create: create,
            resize: resize
        };
    }();

    ShapeShifter.Pause = new Pause();

})(ShapeShifter || (ShapeShifter = {}));