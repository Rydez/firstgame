



var ShapeShifter;
(function(ShapeShifter) {

    var Crosshair = function () {
        this.crosshair;
        this.initWidth = 100;
    	this.initHeight = 100;
        this.heightFactor = 0.05;
        this.widthHeightRatio = this.initWidth/this.initHeight;

        this.xMaxVelFactor = 0.4;
        this.yMaxVelFactor = 0.4;
    };

    Crosshair.prototype = function () {

        var preload = function (game) {
            game.load.spritesheet('crosshair', 'assets/crosshair.png', this.initWidth, this.initHeight);
        };

        var create = function (game) {
            this.crosshair = game.add.sprite(game.width/2, game.height/2, 'crosshair');
            this.crosshair.height = this.heightFactor*game.height;
            this.crosshair.width = this.widthHeightRatio*this.crosshair.height;
            this.crosshair.anchor.setTo(0.5, 0.5);

            game.physics.arcade.enable(this.crosshair);
            this.crosshair.body.collideWorldBounds = true;
            this.crosshair.body.maxVelocity.x = this.xMaxVelFactor*game.width;
            this.crosshair.body.maxVelocity.y = this.yMaxVelFactor*game.height;
        };

        var update = function (game, isMobile) {
            if (!isMobile) {
                this.crosshair.x = game.input.mousePointer.x;
                this.crosshair.y = game.input.mousePointer.y;
            }
        };

        return {
            preload: preload,
            create: create,
            update: update
        };
    }();

    ShapeShifter.Crosshair = new Crosshair();

})(ShapeShifter || (ShapeShifter = {}));
