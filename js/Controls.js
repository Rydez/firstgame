



var ShapeShifter;
(function(ShapeShifter) {

    var Controls = function () {
        this.arrowWidth = 50;
        this.arrowHeight = 100;
        this.arrowHeightFrac = 0.15;
        this.arrowWidthHeightRatio = 2;
        this.arrowEdgeOffsetFrac = 0.05;
        this.arrowSeparationFrac = 0.01;

        this.joyStickWidth = 100;
        this.joyStickHeight = 100;
        this.joyStickHeightFactor = 0.25;
        this.joyStickWidthHeightRatio = this.joyStickWidth / this.joyStickHeight;
        this.xJoyStickAccFactor = 15;
        this.yJoyStickAccFactor = 15;
        this.minCrosshairDistanceFrac = 0.25;

        this.handleWidth = 50;
        this.handleHeight = 50;
        this.handleHeightFactor = 0.2;
        this.handleWidthHeightRatio = this.handleWidth / this.handleHeight;

        this.timeBetweenDoubleTap = 300;

        this.timeUntilCrosshairAcceleration = 0;

        this.adjacent = 0;
        this.opposite = 0;
    };

    Controls.prototype = function () {

        var preload = function (game) {
            game.load.spritesheet('directionArrows', 'assets/directionArrows.png',
                    this.arrowWidth, this.arrowHeight);
            game.load.spritesheet('joyStick', 'assets/jumpButton.png',
                    this.joyStickWidth, this.joyStickHeight);
            game.load.spritesheet('handle', 'assets/handle.png',
                    this.handleWidth, this.handleHeight);
        };

        var create = function (game, isMobile) {

            if (isMobile) {

                // Controls for mobile
                this.leftArrow = game.add.sprite(this.arrowEdgeOffsetFrac*game.width,
                        game.height - this.arrowEdgeOffsetFrac*game.height, 'directionArrows');
                this.leftArrow.height = this.arrowHeightFrac*game.height;
                this.leftArrow.width = this.arrowWidthHeightRatio*this.leftArrow.height;
                this.leftArrow.frame = 0;
                this.leftArrow.inputEnabled = true;
                this.leftArrow.anchor.setTo(0, 1);

                this.rightArrow = game.add.sprite((this.arrowEdgeOffsetFrac + this.arrowSeparationFrac)*game.width + this.leftArrow.width,
                        game.height - this.arrowEdgeOffsetFrac*game.height, 'directionArrows');
                this.rightArrow.height = this.arrowHeightFrac*game.height;
                this.rightArrow.width = this.arrowWidthHeightRatio*this.rightArrow.height;
                this.rightArrow.frame = 1;
                this.rightArrow.inputEnabled = true;
                this.rightArrow.anchor.setTo(0, 1);

                this.joyStick = game.add.sprite(game.width - this.arrowEdgeOffsetFrac*game.width,
                        game.height - this.arrowEdgeOffsetFrac*game.height, 'joyStick');
                this.joyStick.height = this.joyStickHeightFactor*game.height;
                this.joyStick.width = this.joyStickWidthHeightRatio*this.joyStick.height;
                this.joyStick.inputEnabled = true;
                this.joyStick.anchor.setTo(1, 1);

                this.handle = game.add.sprite(this.joyStick.x - this.joyStick.width/2,
                        this.joyStick.y - this.joyStick.height/2, 'handle');
                this.handle.height = this.handleHeightFactor*game.height;
                this.handle.width = this.handleWidthHeightRatio*this.handle.height;
                this.handle.inputEnabled = true;
                this.handle.anchor.setTo(0.5, 0.5);

                this.mobileButtons = {
                    up: this.handle,
                    left: this.leftArrow,
                    right: this.rightArrow
                };
            }
            else {

                // Controls for desktop
                this.keyboardButtons = {
                    up: game.input.keyboard.addKey(Phaser.Keyboard.W),
                    left: game.input.keyboard.addKey(Phaser.Keyboard.A),
                    right: game.input.keyboard.addKey(Phaser.Keyboard.D)
                };
            }
        };

        var update = function (game, crosshair, player, isMobile) {
            // Change direction of the crosshair

            if (!isMobile) {
                return;
            }

            crosshair.x = player.x + this.adjacent;
            crosshair.y = player.y + this.opposite;

            // Check if either pointer is touching
            var pointer1IsTouching = this.handle.input.pointerDown(game.input.pointer1.id);
            var pointer1isHolding = game.input.pointer1.duration > this.timeUntilCrosshairAcceleration;

            var pointer2IsTouching = this.handle.input.pointerDown(game.input.pointer2.id);
            var pointer2isHolding = game.input.pointer2.duration > this.timeUntilCrosshairAcceleration;

            var pointerTouching = null;
            if (pointer1IsTouching && pointer1isHolding) {
                pointerTouching = game.input.pointer1;
            }
            else if (pointer2IsTouching && pointer2isHolding) {
                pointerTouching = game.input.pointer2;
            }
            else {
                this.handle.x = this.joyStick.x - this.joyStick.width/2;
                this.handle.y = this.joyStick.y - this.joyStick.height/2;
                return;
            }

            var xJoyStickCenter = this.joyStick.x - this.joyStick.width/2;
            var yJoyStickCenter = this.joyStick.y - this.joyStick.height/2;
            var tempAdjacent = pointerTouching.x - xJoyStickCenter;
            var tempOpposite = pointerTouching.y - yJoyStickCenter;

            var maxDistance = Math.sqrt(Math.pow(this.joyStick.width/2, 2) +
                    Math.pow(this.joyStick.height/2, 2))
            var minDistance = this.minCrosshairDistanceFrac*this.joyStick.width;

            var distanceFromCenter = Math.sqrt(tempAdjacent*tempAdjacent +
                    tempOpposite*tempOpposite);
            if (distanceFromCenter <= maxDistance) {
                this.adjacent = tempAdjacent;
                this.opposite = tempOpposite;

                this.handle.x = this.joyStick.x - this.joyStick.width/2 + this.adjacent;
                this.handle.y = this.joyStick.y - this.joyStick.height/2 + this.opposite;

                if (distanceFromCenter > minDistance) {
                    crosshair.x = player.x + this.adjacent;
                    crosshair.y = player.y + this.opposite;
                }
                else {
                    this.adjacent = minDistance*tempAdjacent/distanceFromCenter;
                    this.opposite = minDistance*tempOpposite/distanceFromCenter;
                    crosshair.x = player.x + this.adjacent;
                    crosshair.y = player.y + this.opposite;
                }
            }
            else {

                // Maximize
                this.adjacent = maxDistance*tempAdjacent/distanceFromCenter;
                this.opposite = maxDistance*tempOpposite/distanceFromCenter;

                this.handle.x = this.joyStick.x - this.joyStick.width/2 + this.adjacent;
                this.handle.y = this.joyStick.y - this.joyStick.height/2 + this.opposite;
                crosshair.x = player.x + this.adjacent;
                crosshair.y = player.y + this.opposite;
            }
        };

        return {
            preload: preload,
            create: create,
            update: update
        };
    }();

    ShapeShifter.Controls = new Controls();

})(ShapeShifter || (ShapeShifter = {}));
