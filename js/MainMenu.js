



var ShapeShifter;
(function(ShapeShifter) {

    var MainMenu = function () {
    	//
    };

    MainMenu.prototype = function () {

        var preload = function (game) {
            game.load.image('main_menu', 'assets/main_menu.png', 1920, 1080);
        };

        var create = function (game) {
            var mainMenu = game.add.sprite(0, 0, 'main_menu');
            mainMenu.width = game.width;
            mainMenu.height = game.height;
            mainMenu.inputEnabled = true;
            mainMenu.events.onInputDown.add(function () {
                game.state.start('gameplayState');
            }, this);
        };

        return {
            preload: preload,
            create: create
        };
    }();

    ShapeShifter.MainMenu = new MainMenu();

})(ShapeShifter || (ShapeShifter = {}));