




var ShapeShifter;
(function(ShapeShifter) {

    var AbilityManager = function () {
    	this.mobileFireDelay = 200;
    };

    AbilityManager.prototype = function () {

    	var preload = function (game) {
    		ShapeShifter.BulletAbility.preload(game);
    	};

        var create = function (game, player, waveManager) {
    		ShapeShifter.BulletAbility.create(game, player, waveManager);
        };

        var update = function (game, player, enemies, world, crosshair, controls, isMobile) {

            // Don't fire when centered on player
            var oppositeLength = Math.abs(player.y - crosshair.y);
            var adjacentLength = Math.abs(crosshair.x - player.x);
            var fromCenterOffset = 5;
            if (oppositeLength < fromCenterOffset && adjacentLength < fromCenterOffset) {
                return;
            }

            var target1 = game.input.pointer1.targetObject;
            var target2 = game.input.pointer2.targetObject;
            if (isMobile) {

                var pointer1IsTouching = controls.mobileButtons.up.input.pointerDown(game.input.pointer1.id);
                var pointer1isHolding = game.input.pointer1.duration > this.mobileFireDelay;

                var pointer2IsTouching = controls.mobileButtons.up.input.pointerDown(game.input.pointer2.id);
                var pointer2isHolding = game.input.pointer2.duration > this.mobileFireDelay;

                if ((pointer1IsTouching && pointer1isHolding) ||
                    (pointer2IsTouching && pointer2isHolding)) {

                        if (target1 && target1.sprite && target1.sprite.key === 'handle' ||
                            target2 && target2.sprite && target2.sprite.key === 'handle') {
                            ShapeShifter.BulletAbility.update(game, player, enemies, world, crosshair);
                        }
                    }
            }
            else {
                ShapeShifter.BulletAbility.update(game, player, enemies, world, crosshair);
            }
        };

        return {
            preload: preload,
            create: create,
            update: update,
        };
    }();

    ShapeShifter.AbilityManager = new AbilityManager();

})(ShapeShifter || (ShapeShifter = {}));
