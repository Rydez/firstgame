


var ShapeShifter;
(function(ShapeShifter) {

    var Game = new Phaser.Game(1920, 1080, Phaser.AUTO);

    // var introState = {
    //     preload: function () {
    //         ShapeShifter.Intro.preload(Game);
    //     },
    //     create: function () {
    //         ShapeShifter.Intro.create(Game);
    //     }
    // };
    //
    // var mainMenuState = {
    //     preload: function () {
    //         ShapeShifter.MainMenu.preload(Game);
    //     },
    //     create: function () {
    //         ShapeShifter.MainMenu.create(Game);
    //     }
    // };

    var gameplayState = {

        preload: function() {
            ShapeShifter.World.preload(Game);
            ShapeShifter.Player.preload(Game);
            ShapeShifter.Controls.preload(Game);
            ShapeShifter.EnemyManager.preload(Game);
            ShapeShifter.AbilityManager.preload(Game);
            ShapeShifter.Crosshair.preload(Game);
        },

        create: function() {
            Game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            Game.physics.startSystem(Phaser.Physics.ARCADE);
            Game.physics.arcade.TILE_BIAS = 100;

            if (Game.device.desktop) {
                ShapeShifter.isMobile = false;
            }
            else {
                ShapeShifter.isMobile = true;
            }

            ShapeShifter.World.create(Game);
            ShapeShifter.Player.create(Game, ShapeShifter.World,
                    ShapeShifter.WaveManager);
            ShapeShifter.Controls.create(Game, ShapeShifter.isMobile);
            ShapeShifter.Pause.create(Game);
            ShapeShifter.EnemyManager.create(Game);
            ShapeShifter.WaveManager.create(Game, ShapeShifter.EnemyManager);
            ShapeShifter.AbilityManager.create(Game, ShapeShifter.Player.player,
                    ShapeShifter.WaveManager);
            ShapeShifter.Crosshair.create(Game);
        },

        update: function() {
            if (!Game.physics.arcade.checkCollision.left) {
                Game.physics.arcade.checkCollision.left = true;
                Game.physics.arcade.checkCollision.right = true;
                Game.physics.arcade.checkCollision.up = true;
                Game.physics.arcade.checkCollision.down = true;
            }

            ShapeShifter.Player.update(Game, ShapeShifter.World,
                    ShapeShifter.Controls, ShapeShifter.Crosshair.crosshair,
                    ShapeShifter.GroundEnemy.groundEnemyGroup, ShapeShifter.isMobile);
            ShapeShifter.Controls.update(Game, ShapeShifter.Crosshair.crosshair,
                    ShapeShifter.Player.player, ShapeShifter.isMobile);
            ShapeShifter.Crosshair.update(Game, ShapeShifter.isMobile);
            ShapeShifter.AbilityManager.update(Game, ShapeShifter.Player.player,
                    ShapeShifter.GroundEnemy.groundEnemyGroup, ShapeShifter.World,
                    ShapeShifter.Crosshair.crosshair, ShapeShifter.Controls,
                    ShapeShifter.isMobile);
            ShapeShifter.EnemyManager.update(Game, ShapeShifter.Player.player,
                    ShapeShifter.World, ShapeShifter.WaveManager);
        }
    };

    // Game.state.add('introState', introState);
    // Game.state.add('mainMenuState', mainMenuState);
    Game.state.add('gameplayState', gameplayState);

    Game.state.start('gameplayState');

    ShapeShifter.Game = Game;

})(ShapeShifter || (ShapeShifter = {}));
