




var ShapeShifter;
(function(ShapeShifter) {

    var WaveManager = function () {
        this.waves = [
            {

                // Wave 1
                groundEnemies: {
                    count: 4,
                    spawnChance: 1/100
                }
            },
            {

                // Wave 2
                groundEnemies: {
                    count: 8,
                    spawnChance: 1/80
                }
            },
            {

                // Wave 3
                groundEnemies: {
                    count: 15,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 4
                groundEnemies: {
                    count: 26,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 5
                groundEnemies: {
                    count: 34,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 6
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 7
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 8
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 9
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 10
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 11
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 12
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 13
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 14
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 15
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 16
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 17
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            },
            {

                // Wave 18
                groundEnemies: {
                    count: 39,
                    spawnChance: 1/40
                }
            }
        ];

        this.waveStarted = false;
        this.currentWave = -1;

        this.waveInfoSizeFrac = 0.03;
        this.waveInfoOffset = 10;

        this.timeBetweenWaves = 5000;
        this.timeLeftBetween = 5000;

        this.timeLeftOnWave = 15000;
        this.timeOfEachWave = 15000;

        this.restartOccurred = false;
    };

    WaveManager.prototype = function () {

        // Private
        var startWave = function (game, enemyManager) {
            if (this.currentWave < this.waves.length - 1) {
                this.currentWave += 1;
                this.waveStarted = true;
                this.waveInfoText.setText('Wave: ' + (this.currentWave + 1));
                enemyManager.addEnemies(game, this.waves[this.currentWave]);

                this.countDownToWaveEnd(game, enemyManager);
            }
            this.timeLeftBetween = this.timeBetweenWaves;
        };

        var countDownToWaveEnd = function (game, enemyManager) {
            if (this.restartOccurred) {
                this.restartOccurred = false;
                return;
            }

            var secondsLeft = this.timeLeftOnWave/1000;
            this.waveTimeLeftText.setText('Time Left: ' + secondsLeft);
            this.timeLeftOnWave -= 1000;
            if (this.timeLeftOnWave >= 0) {

                var _this = this;
                setTimeout(function () {
                    _this.countDownToWaveEnd(game, enemyManager);
                }, 1000);
            }
            else {
                // this.endWave(game, enemyManager);
                this.waveTimeLeftText.setText('Time Left: --');
            }
        };

        var countDownToNextWave = function (game, enemyManager) {
            if (this.restartOccurred) {
                this.restartOccurred = false;
                return;
            }

            var secondsLeft = this.timeLeftBetween/1000;
            this.waveInfoText.setText('Next wave in: ' + secondsLeft);
            this.timeLeftBetween -= 1000;
            if (this.timeLeftBetween >= 0) {

                var _this = this;
                setTimeout(function () {
                    _this.countDownToNextWave(game, enemyManager);
                }, 1000);
            }
            else {
                this.startWave(game, enemyManager);
            }
        };

        var endWave = function (game, enemyManager) {
            //enemyManager.removeEnemies();
            if (this.currentWave < this.waves.length - 1) {
                this.waveStarted = false;

                this.countDownToNextWave(game, enemyManager);
            }
            else {
                this.waveStarted = false;
                this.waveInfoText.setText('All waves are completed!');
            }
            this.timeLeftOnWave = this.timeOfEachWave;
        };

        var restartWaves = function () {
            this.restartOccurred = true;
            this.currentWave = -1;
            this.timeLeftBetween = this.timeBetweenWaves;
            this.timeLeftOnWave = this.timeOfEachWave;
        };

        // Public
        var create = function (game, enemyManager) {
            var style = { font: 'Arial', fontSize: this.waveInfoSizeFrac*game.height, fill: '#000000' };
            this.waveInfoText = game.add.text(game.width - this.waveInfoOffset, this.waveInfoOffset, '', style);
            this.waveInfoText.anchor.setTo(1, 0);

            this.waveTimeLeftText = game.add.text(this.waveInfoOffset, this.waveInfoOffset, 'Time Left: --', style);
            this.waveTimeLeftText.anchor.setTo(0, 0);

            var _this = this;
            setTimeout(function () {
                _this.countDownToNextWave(game, enemyManager);
            }, 1000);
        };

        var update = function (game, enemyManager) {
            if (this.waveStarted && this.timeLeftOnWave <= 0) {
                this.endWave(game, enemyManager);
            }
            else if (this.waveStarted && this.timeLeftOnWave > 0) {
                this.timeOfEachWave -= 1000;
            }
        };

        return {
            startWave: startWave,
            countDownToNextWave: countDownToNextWave,
            countDownToWaveEnd: countDownToWaveEnd,
            endWave: endWave,
            restartWaves: restartWaves,
            create: create,
            update: update
        };
    }();

    ShapeShifter.WaveManager = new WaveManager();

})(ShapeShifter || (ShapeShifter = {}));
