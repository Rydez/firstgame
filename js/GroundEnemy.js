


var ShapeShifter;
(function(ShapeShifter) {

    var GroundEnemy = function () {
    	this.initWidth = 32;
    	this.initHeight = 32;
    	this.heightFactor = 0.045;
    	this.widthHeightRatio = 32/32;

        this.enemyMass = 3;

    	this.xVelFactor = 0.8;
        this.xAccFactor = 1.5;

        this.xStoppingVel = 15;

    	this.gravityFactor = 0.5;

    	this.jumpChance = 1/100;
    	this.spawnChance = 0;

    	this.numberOfEnemies = 0;

    	this.groundEnemyGroup;

    	this.numberSpawned = 0;
    };

    GroundEnemy.prototype = function () {

    	var preload = function (game) {

    		// Must have 'Enemy' in name to be damaged
			game.load.spritesheet('groundEnemy', 'assets/baddie.png', this.initWidth, this.initHeight);
    	};

        var create = function (game) {
			this.groundEnemyGroup = game.add.group();
        };

        var addEnemies = function (game, wave) {
			this.groundEnemyGroup.removeBetween(0, this.groundEnemyGroup.length - 1, true, true);
            this.spawnChance = wave.groundEnemies.spawnChance;
            this.numberOfEnemies = wave.groundEnemies.count;
            this.numberSpawned = 0;
			for (var i = 0; i < this.numberOfEnemies; i++) {
				var enemy = game.add.sprite(game.width, game.height, 'groundEnemy');
				this.groundEnemyGroup.add(enemy);
			    enemy.anchor.setTo(0.5, 0.5);

			    //  We need to enable physics on the enemy
			    game.physics.arcade.enable(enemy);

			    enemy.body.gravity.y = this.gravityFactor*game.height;
                enemy.body.mass = this.enemyMass;
                enemy.health = 100;

                enemy.body.checkCollision.up = false;

			    //  Our two animations, bounce left and right.
			    enemy.animations.add('left', [1, 0], 4, false);
			    enemy.animations.add('right', [2, 3], 4, false);

			    enemy.kill();
			}
        };

        // var removeEnemies = function () {
        //     this.groundEnemyGroup.removeBetween(0, this.groundEnemyGroup.length - 1, true, true);
        // };

        var update = function (game, player, world, waveManager) {
            game.physics.arcade.collide(this.groundEnemyGroup, world.platformGroup);
		    game.physics.arcade.collide(this.groundEnemyGroup, player);

            // Try to spawn for certain draw
			var spawnDraw = Math.floor(Math.random() * (1/this.spawnChance) + 1);
			if (spawnDraw === 1 &&
                waveManager.waveStarted &&
                waveManager.timeLeftOnWave >= 0 &&
                this.numberSpawned < this.numberOfEnemies) {

				var enemy = this.groundEnemyGroup.children[this.numberSpawned];
				this.numberSpawned += 1;

				if (enemy !== null && enemy !== undefined) {
					enemy.revive();

				    // Use this offset to start baddies at some x
                    var xMin = world.groundPlatform.x - world.groundPlatform.width/2;
                    var xMax = world.groundPlatform.x + world.groundPlatform.width/2;
				    var xStart = Math.floor(Math.random() * (xMax - xMin + 1) + xMin);

    			    enemy.height = this.heightFactor*game.height;
				    enemy.width = this.widthHeightRatio*enemy.height;
    			    enemy.x = xStart;
    			    enemy.y = -enemy.height;
				}
			}

			var allEnemies = this.groundEnemyGroup.children;
        	for (var i = 0; i < allEnemies.length; i++) {

                // Kill out of bound enemies
                if (!world.background.getBounds().contains(allEnemies[i].x, allEnemies[i].y) &&
                    allEnemies[i].y > 0) {
                    allEnemies[i].kill();
                }

                // Try to charge for certain draws
        		if (allEnemies[i].alive && allEnemies[i].body.touching.down) {

					var chargeDraw = Math.floor(Math.random() * (1/this.jumpChance) + 1);

					if (chargeDraw === 1) {

						if (player.x > allEnemies[i].x) {
							allEnemies[i].body.velocity.x = this.xVelFactor * game.width;
							allEnemies[i].animations.play('right');
						}
						else {
							allEnemies[i].body.velocity.x = - this.xVelFactor * game.width;
							allEnemies[i].animations.play('left');
						}
					}
        		}

                // Slow down and stop
                if (Math.abs(allEnemies[i].body.velocity.x) < this.xStoppingVel) {
                    allEnemies[i].animations.stop();
                    allEnemies[i].body.velocity.x = 0;
                    allEnemies[i].body.acceleration.x = 0;
                }
                else if (allEnemies[i].body.velocity.x > 0) {
                    allEnemies[i].body.acceleration.x = -this.xAccFactor*game.width;
                }
                else if (allEnemies[i].body.velocity.x < 0) {
                    allEnemies[i].body.acceleration.x = this.xAccFactor*game.width;
                }

			    // Check for collision pass throughs
                if (world.groundPlatform.getBounds().contains(allEnemies[i].x, allEnemies[i].y)) {
			    	this.allEnemies[i].y = world.groundPlatform.y - (this.allEnemies[i].height/2 + world.groundPlatform.height/2);
			    }

			    // Kill enemies that somehow get pushed out of bounds
			    if (allEnemies[i].alive &&
			    	(allEnemies[i].x <= -allEnemies[i].width/2 || allEnemies[i].x >= game.width + allEnemies[i].width/2)) {
			    	allEnemies[i].kill();
			    }

				allEnemies[i].xBeforeResize = allEnemies[i].x / game.width;
				allEnemies[i].yBeforeResize = allEnemies[i].y / game.height;

				allEnemies[i].xVelBeforeResize = allEnemies[i].body.velocity.x / game.width;
				allEnemies[i].yVelBeforeResize = allEnemies[i].body.velocity.y / game.height;
			}
        };

        return {
        	preload: preload,
            create: create,
            update: update,
            addEnemies: addEnemies,
            // removeEnemies: removeEnemies
        };
    }();

    ShapeShifter.GroundEnemy = new GroundEnemy();

})(ShapeShifter || (ShapeShifter = {}));
