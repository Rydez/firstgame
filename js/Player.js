



var ShapeShifter;
(function(ShapeShifter) {

    var Player = function () {
    	this.initPlayerWidth = 81;
    	this.initPlayerHeight = 125;
    	this.playerHeightFactor = 0.09;
    	this.playerWidthHeightRatio = this.initPlayerWidth / this.initPlayerHeight;
        this.xPlayerAnchor = 0.5;
        this.yPlayerAnchor = 0.5;

        this.yPlayerHeadOffset = 20;
        this.xPlayerHeadOffset = 2;
        this.xHeadAnchor = 0.5;
        this.yHeadAnchor = 0.31;

        this.initGunWidth = 101;
        this.yPlayerGunOffset = 0;
        this.xPlayerGunOffset = -10;
        this.xRightGunAnchor = 0.34;
        this.xLeftGunAnchor = 0.66;
        this.yGunAnchor = 0.5;

    	this.xAccFactor = 1.3;
    	this.xMaxVelFactor = 0.3;
    	this.yVelFactor = 1.5;

    	// If the player is slower than this velocity, then stop
    	this.xStoppingVel = 15;

    	this.gravityFactor = 3;

    	this.jumpKeyIsReset = true;
    };

    Player.prototype = function () {

    	var preload = function (game) {
            game.load.audio('teleport', 'assets/teleport.mp3');
			game.load.spritesheet('energyPlayer', 'assets/energyPlayer.png', this.initPlayerWidth, this.initPlayerHeight);
			game.load.spritesheet('playerHead', 'assets/headSheet.png', this.initPlayerWidth, this.initPlayerHeight);
			game.load.spritesheet('playerArms', 'assets/gunSheet.png', this.initGunWidth, this.initPlayerHeight);
    	};

        var create = function (game, world, waveManager) {
        	this.teleportSound = game.add.audio('teleport');

		    // Main player sprite container
			this.playerSpriteGroup = game.add.group();
            this.playerSpriteGroup.alpha = 0;

		    // Main player sprite
		    this.player = game.add.sprite(game.width, game.height, 'energyPlayer');
            this.player.height = this.playerHeightFactor*game.height;
		    this.player.width = this.playerWidthHeightRatio*this.player.height;
            this.player.x = world.groundPlatform.x;
		    this.player.y = world.groundPlatform.y -
                    (this.player.height/2 + world.groundPlatform.height/2);
		    this.player.anchor.setTo(this.xPlayerAnchor, this.yPlayerAnchor);

		    // Player's head
		    this.playerHead = game.add.sprite(this.player.x + this.xPlayerHeadOffset, this.player.y - this.yPlayerHeadOffset, 'playerHead');
            this.playerHead.width = this.player.width;
		    this.playerHead.height = this.player.height;
		    this.playerHead.anchor.setTo(this.xHeadAnchor, this.yHeadAnchor);
            this.playerHead.frame = 0;

		    // // Player's arms (weapon)
		    this.playerArms = game.add.sprite(this.player.x + this.xPlayerGunOffset, this.player.y - this.yPlayerGunOffset, 'playerArms');
            this.playerArms.width = this.player.width;
		    this.playerArms.height = this.player.height;
		    this.playerArms.anchor.setTo(this.xRightGunAnchor, this.yGunAnchor);
            this.playerArms.frame = 0;

		    // Add head and arms to parent player container
		    this.playerSpriteGroup.add(this.playerHead);
		    this.playerSpriteGroup.add(this.player);
		    this.playerSpriteGroup.add(this.playerArms);

		    // Fade player in after a delay
		    var enterFade = game.add.tween(this.playerSpriteGroup).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 1000);
            enterFade.onStart.add(function () {
    			this.teleportSound.play();
            }, this);

            this.player.checkWorldBounds = true;
            this.player.events.onOutOfBounds.add(function () {
                var _this = this;
                var checkPlayerDeath = function () {
                    if (_this.player.y > game.height + _this.player.height) {
                        waveManager.restartWaves();
                        game.state.restart();
                    }
                };
                setTimeout(checkPlayerDeath, 1000);
            }, this);

		    // We need to enable physics on the player
		    game.physics.arcade.enable(this.playerSpriteGroup);

            this.player.body.gravity.y = this.gravityFactor*game.height;
            this.playerHead.body.gravity.y = this.gravityFactor*game.height;
		    this.playerArms.body.gravity.y = this.gravityFactor*game.height;

            this.player.body.maxVelocity.x = this.xMaxVelFactor*game.width;
            this.playerHead.body.maxVelocity.x = this.xMaxVelFactor*game.width;
		    this.playerArms.body.maxVelocity.x = this.xMaxVelFactor*game.width;

            this.player.body.checkCollision.up = false;
            this.playerHead.body.checkCollision.up = false;
            this.playerArms.body.checkCollision.up = false;

		    // player animations
            this.player.animations.add('right', [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 20, true);
            this.player.animations.add('rightBackwards', [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29], 20, true);
		    this.player.animations.add('left', [29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18], 20, true);
            this.player.animations.add('leftBackwards', [14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3], 20, true);
        };

        var update = function (game, world, controls, crosshair, enemies, isMobile) {
            game.physics.arcade.collide(this.playerSpriteGroup, world.platformGroup);
            game.physics.arcade.collide(this.playerSpriteGroup, enemies);

            // Check for collision pass throughs
            var passThroughBuffer = 5
		    if (world.groundPlatform.getBounds().contains(this.player.x, this.player.y)) {
                this.player.y = world.groundPlatform.y - (this.player.height/2 + world.groundPlatform.height/2);
		    }

            // Rotate face and arms to follow the pointer
            var opposite = this.playerArms.y - crosshair.y;
            var adjacent = crosshair.x - this.playerArms.x;
            var theta = Math.atan(Math.abs(opposite / adjacent)) * (180/Math.PI);

            // Create angle offset based on x crosshair position for setting the angle
            if (adjacent < 0) {
                var adjOffset = 2 * (90 - theta) - 180;
                this.playerHead.frame = 1;
                this.playerArms.frame = 1;
            }
            else {
                var adjOffset = 0;
                this.playerHead.frame = 0;
                this.playerArms.frame = 0;
            }

            // Set the angle
            if (opposite < 0) {
                this.playerHead.angle = adjOffset + theta;
                this.playerArms.angle = adjOffset + theta;
            }
            else {
                this.playerHead.angle = 360 - (adjOffset + theta);
                this.playerArms.angle = 360 - (adjOffset + theta);
            }

            if (isMobile && controls.mobileButtons) {
                var xPointer1 = game.input.pointer1.x;
                var yPointer1 = game.input.pointer1.y;

                var xPointer2 = game.input.pointer2.x;
                var yPointer2 = game.input.pointer2.y;

                var leftBounds = controls.mobileButtons.left.getBounds();
                var rightBounds = controls.mobileButtons.right.getBounds();

                var leftIsDown = (game.input.pointer1.isDown && leftBounds.contains(xPointer1, yPointer1)) ||
                        (game.input.pointer2.isDown && leftBounds.contains(xPointer2, yPointer2));

                var rightIsDown = (game.input.pointer1.isDown && rightBounds.contains(xPointer1, yPointer1)) ||
                        (game.input.pointer2.isDown && rightBounds.contains(xPointer2, yPointer2));

                var upIsDown = (controls.mobileButtons.up.input.pointerDown(game.input.pointer1.id) &&
                        game.input.pointer1.msSinceLastClick < controls.timeBetweenDoubleTap) ||
                        (controls.mobileButtons.up.input.pointerDown(game.input.pointer2.id) &&
                        game.input.pointer2.msSinceLastClick < controls.timeBetweenDoubleTap);
            }
            else if (!isMobile && controls.keyboardButtons) {
                var leftIsDown = controls.keyboardButtons.left.isDown;
                var rightIsDown = controls.keyboardButtons.right.isDown;
                var upIsDown = controls.keyboardButtons.up.isDown;
            }

            // Move left
		    if (leftIsDown && !rightIsDown) {
                this.player.body.acceleration.x = -this.xAccFactor*game.width;
                this.playerHead.body.acceleration.x = -this.xAccFactor*game.width;
                this.playerArms.body.acceleration.x = -this.xAccFactor*game.width;

                if (adjacent < 0) {
                    this.player.animations.play('left');
                }
                else {
                    this.player.animations.play('leftBackwards');
                }
		    }

            // Move Right
		    if (rightIsDown && !leftIsDown) {
                this.player.body.acceleration.x = this.xAccFactor*game.width;
                this.playerHead.body.acceleration.x = this.xAccFactor*game.width;
                this.playerArms.body.acceleration.x = this.xAccFactor*game.width;

                if (adjacent < 0) {
                    this.player.animations.play('rightBackwards');
                }
                else {
                    this.player.animations.play('right');
                }
		    }

            //  Stand still
		    if ((!leftIsDown && !rightIsDown) || (leftIsDown && rightIsDown)) {

                if (adjacent < 0) {
                    this.player.frame = 15
                }
                else {
                    this.player.frame = 2;
                }

		        if (Math.abs(this.player.body.velocity.x) < this.xStoppingVel) {
                    this.player.body.acceleration.x = 0;
                    this.playerHead.body.acceleration.x = 0;
                    this.playerArms.body.acceleration.x = 0;
                    this.player.body.velocity.x = 0;
                    this.playerHead.body.velocity.x = 0;
		        	this.playerArms.body.velocity.x = 0;
		        }
		        else if (this.player.body.velocity.x > 0) {
                    this.player.body.acceleration.x = -this.xAccFactor*game.width;
                    this.playerHead.body.acceleration.x = -this.xAccFactor*game.width;
		    		this.playerArms.body.acceleration.x = -this.xAccFactor*game.width;
		        }
		        else if (this.player.body.velocity.x < 0) {
                    this.player.body.acceleration.x = this.xAccFactor*game.width;
                    this.playerHead.body.acceleration.x = this.xAccFactor*game.width;
		        	this.playerArms.body.acceleration.x = this.xAccFactor*game.width;
		        }
		    }

		    //  Jump if key is reset and player is on something
		    if (upIsDown && this.jumpKeyIsReset && this.player.body.touching.down) {
                this.player.body.velocity.y = -this.yVelFactor*game.height;
                this.playerHead.body.velocity.y = -this.yVelFactor*game.height;
		        this.playerArms.body.velocity.y = -this.yVelFactor*game.height;
		        this.jumpKeyIsReset = false;
		    }
		    else if (!upIsDown && !this.jumpKeyIsReset) {
		    	this.jumpKeyIsReset = true;
		    }

            if (adjacent < 0) {
                this.playerHead.x = this.player.x - this.xPlayerHeadOffset;
                this.playerArms.x = this.player.x - this.xPlayerGunOffset;
                this.playerArms.anchor.setTo(this.xLeftGunAnchor, this.yGunAnchor);
            }
            else {
                this.playerHead.x = this.player.x + this.xPlayerHeadOffset;
                this.playerArms.x = this.player.x + this.xPlayerGunOffset;
                this.playerArms.anchor.setTo(this.xRightGunAnchor, this.yGunAnchor);
            }
            this.playerHead.y = this.player.y - this.yPlayerHeadOffset
            this.playerArms.y = this.player.y - this.yPlayerGunOffset
        };

        return {
            preload: preload,
            create: create,
            update: update
        };
    }();

    ShapeShifter.Player = new Player();

})(ShapeShifter || (ShapeShifter = {}));
