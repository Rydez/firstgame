



var ShapeShifter;
(function(ShapeShifter) {

    var Intro = function () {
        this.logoWidthFrac = 0.25;
    	this.logoHeightFrac = 0.135;
    };

    Intro.prototype = function () {

        var preload = function (game) {
            game.load.image('intro_background', 'assets/intro_background.png', 1920, 1080);
            game.load.image('logo', 'assets/logo.png', 487, 151);
        };

        var create = function (game) {
            var introBackground = game.add.sprite(0, 0, 'intro_background');
            introBackground.width = game.width;
            introBackground.height = game.height;

            var logo = game.add.sprite(game.world.centerX, game.world.centerY, 'logo');
            logo.width = this.logoWidthFrac*game.width;
            logo.height = this.logoHeightFrac*game.height;
            logo.anchor.set(0.5);

            var introBounce = game.add.tween(logo).from( { y: -200 }, 2000, Phaser.Easing.Bounce.Out, false);
            var introFade = game.add.tween(logo).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, false);
            
            introBounce.chain(introFade);

            introBounce.start();

            introFade.onComplete.add(function () {
                game.state.start('mainMenuState');
            }, this);
        };

        return {
            preload: preload,
            create: create
        };
    }();

    ShapeShifter.Intro = new Intro();

})(ShapeShifter || (ShapeShifter = {}));