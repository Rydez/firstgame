



var ShapeShifter;
(function(ShapeShifter) {

    var BulletAbility = function () {
    	this.initWidth = 32;
    	this.initHeight = 28;
    	this.heightFactor = 0.03;
    	this.widthHeightRatio = this.initWidth/this.initHeight;

    	this.xVelFactor = 0.06;
    	this.yVelFactor = 0.4;

    	this.numberOfBullets = 100;

    	this.speedFactor = 0.8;
    	this.rate = 150;
    	this.nextFire = 0;
    	this.lifeSpan = 2000;
    	this.dmg = 50;
    	this.weapon;
    	this.shootSound;
    };

    BulletAbility.prototype = function () {

    	var preload = function (game) {
    		game.load.audio('shoot', 'assets/shoot.mp3');
    		game.load.audio('break', 'assets/break.mp3');
			game.load.spritesheet('diamond', 'assets/diamond.png', this.initWidth, this.initHeight);
    	};

        var create = function (game, player, waveManager) {
            this.waveInfoSizeFrac = waveManager.waveInfoSizeFrac;
            this.waveInfoOffset = waveManager.waveInfoOffset;

        	this.shootSound = game.add.audio('shoot');
        	this.breakSound = game.add.audio('break');

			this.weapon = game.add.weapon(this.numberOfBullets, 'diamond');
			this.weapon.bulletKillType = Phaser.Weapon.KILL_LIFESPAN;
			this.weapon.bulletSpeed = this.speedFactor*game.width;
			this.weapon.fireRate = this.rate;
			this.weapon.bulletLifespan = this.lifeSpan;
    		this.weapon.bulletAngleOffset = 90;
			this.weapon.trackSprite(player);

			this.weapon.onFire.add(function () {
	    		this.shootSound.play();
			}, this);

			var _this = this;
        	this.weapon.bullets.forEach(function (bullet) {
        		bullet.height = _this.heightFactor*game.height;
        		bullet.width = _this.widthHeightRatio*bullet.height;
        		bullet.body.updateBounds();
        	});
        };

        var update = function (game, player, enemies, world, crosshair) {

        	var _this = this;
        	var bulletCollsion = function (bullet, otherSprite) {
        		_this.breakSound.play();
        		bullet.kill();
        		if (otherSprite.key.indexOf('Enemy') > -1) {
        			otherSprite.damage(_this.dmg);
        		}
        	};

		    game.physics.arcade.collide(this.weapon.bullets, enemies, bulletCollsion);
		    game.physics.arcade.collide(this.weapon.bullets, world.platformGroup, bulletCollsion);

		    if (game.input.activePointer.isDown) {
		    	var opposite = player.y - crosshair.y;
		    	var adjacent = crosshair.x - player.x;
		    	var theta = Math.atan(Math.abs(opposite / adjacent)) * (180/Math.PI);

		    	if (adjacent < 0) {
		    		var adjOffset = 2 * (90 - theta);
		    	}
		    	else {
		    		var adjOffset = 0;
		    	}

		    	if (opposite < 0) {
		    		this.weapon.fireAngle = adjOffset + theta;
		    	}
		    	else {
		    		this.weapon.fireAngle = 360 - (adjOffset + theta);
		    	}

		    	this.weapon.fire();
		    }
        };

        return {
        	preload: preload,
            create: create,
            update: update
        };
    }();

    ShapeShifter.BulletAbility = new BulletAbility();

})(ShapeShifter || (ShapeShifter = {}));
